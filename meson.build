#we set the cpp_std to none, so that we can manually set it. We do that, because
#meson complains about c++latest, otherwise
project('Lithekey', 'cpp', meson_version: '>=0.57.0', license:'MIT',
	version: files('Code/Version/VersionManifest'))

#First, we need to know what compiler we have, so that we can pass arguments accordingly
compilerCPPObject = meson.get_compiler('cpp')

# Here, we store what form of arguments the compiler takes, as well as the underlying
# compiler, for any fine-tuning
compiler_used_array = ['Unknown', 'Unknown']

if compilerCPPObject.get_id() == 'msvc'

	compiler_used_array = ['msvc', 'msvc']

elif compilerCPPObject.get_id() == 'clang-cl'

	compiler_used_array = ['msvc', 'clang']

elif compilerCPPObject.get_argument_syntax() == 'msvc'

	compiler_used_array = ['msvc', 'Unknown']

elif compilerCPPObject.get_argument_syntax() == 'gcc'

	compiler_used_array = ['gcc', 'Unknown']

endif

# Setup global arguments, since these must be done first, before any target is built. Have
# to check we're not a subproject, since add_global_arguments only works for a top-level
# project, and there's no other way I know of passing arguments to subprojects
if not meson.is_subproject()

	if compiler_used_array[0] == 'msvc'

		if compiler_used_array[1] == 'clang'

			# Disable _CRT_SECURE_NO_WARNINGS warnings, since the icu emits them. At time of
			# writing, meson's warning_level=0, in the icu default options above doesn't stop
			# them for clang-cl, so we use this argument instead. We have to do this for both
			# cpp and c, as icu has both
			add_global_arguments('-Wno-deprecated', language: 'cpp')
			add_global_arguments('-Wno-deprecated', language: 'c')

		endif

	endif

endif

targetOS = build_machine.system()

Powershell = find_program('pwsh', required: true)

#Get the specified frao dir, from either this project, or its superproject
frao_dir = get_option('frao_dir')

#if we have no frao_dir, then we should assume use of the .frao directory in this (sub)project
if frao_dir == ''

	frao_dir = join_paths(meson.project_source_root(), '.frao')

endif

# Find nucleus subproject dependency, and its include files
nucleus_proj = subproject('nucleus', required: true)
nucleus_dep = nucleus_proj.get_variable('nucleus_dep')
nucleus_script_dir = nucleus_proj.get_variable('nucleus_script_dir')

# Find protean subproject dependency, and its include files
protean_proj = subproject('protean', required: true)
protean_dep = protean_proj.get_variable('protean_dep')

versionFull = meson.project_version()
versionSegmentArray = versionFull.split('.')

if versionSegmentArray.length() < 5

	error('Could not retreive Valid Version information')

endif

versionShort = '.'.join([versionSegmentArray.get(0), versionSegmentArray.get(1)])

# Get a number corresponding to (char1 << 8) | char2. Done here since it's easier than doing
# it in the c++ preproccesor
versionStatusDict = {'dv': 25718, 'rc': 29283}
versionStatusNumber = versionStatusDict.get(versionSegmentArray.get(2), 65535)

pathsWriterScript = files(join_paths(nucleus_script_dir, 'MesonPathsWriter.ps1'))
run_target('Build_Paths', command: [Powershell, '-executionpolicy', 'remotesigned', '-file',
	pathsWriterScript, '-FraoDir', frao_dir, '-ScriptsDir', nucleus_script_dir])

#summarise basic frao options for the user
summary({
	'API Short Version': versionShort,
	'API Full Version': versionFull,
	'.frao dir': frao_dir,
	'scripts dir': nucleus_script_dir},
	section: 'frao options')

# Set data, including version, that needs to set in various (normally documentation)
# files
conf_data = configuration_data()
conf_data.set('Version_Short', versionShort)
conf_data.set('Version_Long', versionFull)
conf_data.set('Version_Major', versionSegmentArray.get(0))
conf_data.set('Version_Minor', versionSegmentArray.get(1))
conf_data.set('Version_Status', versionSegmentArray.get(2))
conf_data.set('Version_Status_Number', versionStatusNumber)
conf_data.set('Version_Patch', versionSegmentArray.get(3))
conf_data.set('Version_DevBuild', versionSegmentArray.get(4))
conf_data.set('Copyright', '\'2022, Freya Rhiannon Mayger\'')
conf_data.set('Author', '\'Freya Rhiannon Mayger\'')

# Work out if we should build documentation with dot graphs
dot_executable = find_program('dot', required: false)
if dot_executable.found()

	conf_data.set('Have_Dot', 'YES')
	conf_data.set('Dot_Path', dot_executable.full_path())

else

	conf_data.set('Have_Dot', 'NO')
	conf_data.set('Dot_Path', '')

endif

# Get the configuration of the current build
frao_debug_symbols = get_option('frao_debug_symbols')
frao_cpp_std = get_option('frao_cpp_std')
meson_debug = get_option('debug')
meson_disabled_asserts = (get_option('b_ndebug') == 'true')
meson_warning_level = get_option('warning_level')
meson_optimisation_level = get_option('optimization')

# Get the library type building
default_library = get_option('default_library')

lithekey_export_def = []
lithekey_import_def = []

if compiler_used_array[0] == 'msvc'

	# Export for dynamic library (dll etc) building
	lithekey_export_def = ['/DLITHEKEY_EXPORT']

	# Prepare the correct define, for user of this api, so that they can simply
	# acquire this variable
	if default_library != 'static'

		lithekey_import_def += '/DLITHEKEY_IMPORT'

	endif 

	# We always want the highest non-Wall warning level. (since Wall is not intended
	# for normal use)
	add_project_arguments('/W4', language : 'cpp')

	# We always want unicode defined, whether that's in windows headers (UNICODE),
	# or in c runtime headers (_UNICODE)
	add_project_arguments('/D_UNICODE', language : 'cpp')
	add_project_arguments('/DUNICODE', language : 'cpp')

	#Make sure that visual studio actually updates the __cplusplus macro to have the correct value
	add_project_arguments('/Zc:__cplusplus', language: 'cpp')

	# We default to the option we want in release builds.
	# For release builds, we want the full pdb linking option, since release builds
	# might be deployed, and we may not have all the data locally. Note that
	# frao_debug_symbols must be set to true, for this to have any effect
	debug_symbol_style = 'FULL'

	# Predefine, outside of the if
	config_data_defines = 'UNICODE _UNICODE _MT'

	if meson_disabled_asserts

		config_data_defines += ' NDEBUG'

	endif

	if meson_debug

		# Add predefined symbols to our list of them
		config_data_defines += ' _DEBUG'

		# For debug builds, we want the fast pdb linking option, since debug builds
		# won't be deployed, and we have all the data locally. Note that
		# frao_debug_symbols must be set to true, for this to have any effect
		debug_symbol_style = 'FASTLINK'

	endif

	# Set c++ version manually, bc at time of writing, relying on meson doesn't
	# work on VC++, so we always set manually
	vs_cpp_std = '/std:c++' + frao_cpp_std.to_string()
	add_project_arguments(vs_cpp_std, language: 'cpp')

	# As far as I'm aware, only the actual cl compiler supports these arguments
	if compiler_used_array[1] == 'msvc'

		#enforces that fully qualified paths are returned
		add_project_arguments('/FC', language : 'cpp')
		
	endif

	if compiler_used_array[1] == 'clang'

		# We need to make sure that doxygen and such know if we used clang
		config_data_defines += ' __clang__'

		# Clang doesn't support fastlink
		debug_symbol_style = 'FULL'

	endif

	# Tell things that use our config data (doxygen), what things we have predefined
	conf_data.set('Predefined', config_data_defines)

	if frao_debug_symbols

		# compiler 'produce pdb debug symbols' flag, for cl.exe
		add_project_arguments('/Zi', language: 'cpp')
		# linker 'produce pdb debug file, for specified symbols' flag, for link.exe
		# (or whatever cl.exe's linker is called)
		add_project_link_arguments('/DEBUG:' + debug_symbol_style, language : 'cpp')

	endif

elif compiler_used_array[0] == 'gcc'

	error('You appear to be using a gcc-style compiler, which is not supported at ' +
		'this time')

else

	error('Unrecognised compiler, with unknown argument form. Please update project, ' +
		'including root meson.build with details of this compiler')

endif

#summarise compilations options for the user
summary({
	'compiler argument style': compiler_used_array[0],
	'underlying compiler': compiler_used_array[1],
	'target OS': targetOS,
	'debug build': meson_debug,
	'debug symbols': frao_debug_symbols,
	'disabled asserts': meson_disabled_asserts,
	'(meson) optimisation level': meson_optimisation_level,
	'(meson) warning level': meson_warning_level,
	'used c++ version': frao_cpp_std},
	section: 'compilation options')

extra_sources = []

versionHeader = configure_file(input: files('Includes/Lithekey_inc/Version/LithekeyVersion.h.in'),
	output: 'LithekeyVersion.h', configuration : conf_data)
lithekeyBuildPath = meson.project_build_root()

if targetOS == 'windows'

	# compile resource info - for version etc.
	ResourceCompiler = find_program('RC')
	Ver_Info = custom_target('Version_Info',
					input: ['Includes\Lithekey_inc\Resources.rc', versionHeader,
						'Includes\Lithekey_inc\Version\Version.h'],
					output : 'Resource.res',
					command : [ResourceCompiler, '/I', lithekeyBuildPath, '/fo', '@OUTPUT@', '@INPUT0@'])

	extra_sources += Ver_Info

endif

# get header files, by including top-level of header directory tree
# we include only this directory, because we use a fully-qualified header style
include_dirs_internal = include_directories('Includes/Lithekey_inc', lithekeyBuildPath)
include_dirs_external = include_directories('Includes', lithekeyBuildPath)

#get source files, recursively
subdir('Code')

lib_cpp_defines = [lithekey_export_def]

#build actual library
lib_lithekey = library('Lithekey', [source_files, extra_sources],
		dependencies: [nucleus_dep, protean_dep], include_directories : [include_dirs_internal],
		cpp_args: [lib_cpp_defines], name_prefix : 'lib__', install: true)

#declare dependency, for using this project as a subproject
lithekey_dep = declare_dependency(link_with: lib_lithekey, include_directories: include_dirs_external)

#we need to pass our header files over
install_subdir('Includes', install_dir: 'Includes', strip_directory : true)

#go into documentation section; create documentation
subdir('Documentation')

#go into tests section; do tests
subdir('Test')
