# Introduction and Setup

*Lithekey* is the frao input API, taking and routing keyboard and mouse input. 

## Structure
For the Lithekey API, the API is primarily divided into the InputManager class, which handles takes window input messages and translates them, and the MessageQueues, which take specified types of input, and send them wherever required. In addition to these classes, there are several smaller types which hold actual input message data, in a standardised way.

## Setup
Lithekey, as an API, is designed to be used as a meson subproject. In that case, one needs simply to use as any other meson subproject. If one wishes to use Lithekey as a primary project however, and use its scripts, extra care must be taken.

First, one should download/clone the Lithekey gitlab repo, to a suitable location. Since Lithekey uses out of source builds, as is typical with meson, the source file should be located next to a seperate build directory.

There are three ways of setting up Lithekey, as a primary project (ie: not a subproject) with the ability to run scripts. They are:
1. [With VSCode](#with-vscode), running setup scripts pre-configured, using vscode tasks
2. [With Powershell Scripts](#with-powershell-scripts), running the powershell scripts yourself, outside of vscode
3. [Manually](#manually), setting up meson build directory yourself, and building your own paths

### With VSCode
For VSCode, simply open the Lithekey source folder in VSCode, and run the 'Generate Build folders' task. It will setup a default meson build structure, in ../Build/, as well as writing required build and script data to the .frao directory. The default build structure will be based on current operating system, and installed compilers. Assuming it runs successfully, you will now be setup for running any Lithekey scripts, via the other tasks. 

### With Powershell Scripts
In this case, you will have to run the SetupBuildSystem.ps1 powershell script, from the .frao directory. For ths script, the following parameters are mandatory:
- 'SourceFolder'; this is the location of repo folder itself
- 'BuildFolder'; this is the location of the build folder, into which all build artifacts, libraries, and documentation will be built
- 'InstallType'; one of 'NoInstall' (for using meson install defaults), 'Install' (for installing to root/projectname/os/compiler/config), or 'NoOSInstall' (for installing to root/projectname/compiler/config). If you do not intend to install the API anywhere, just pass NoInstall.
- 'DefaultLibrary'; This is the default type of library to build. Either shared (.so/.dll), static (.lib/.a), or both.

### Manually
If you setup vscode manually, you simply set up the meson build folders as you would for any other project, except that after you have, you should compile the ./Build_Paths target. This will create the proper script information in the .frao directory