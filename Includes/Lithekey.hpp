#ifndef FRAO_LITHEKEY_TOPINCL
#define FRAO_LITHEKEY_TOPINCL

//! \file
//!
//! \brief Header of entire Lithekey api
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "KeyCodes/Codes.hpp"
#include "Types/Base.hpp"
#include "Types/Controllers.hpp"
#include "Manager/InputManager.hpp"
#include "Version/VersionIncl.hpp"

#endif