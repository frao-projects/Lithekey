#ifndef FRAO_LITHEKEY_MANAGER_INPUT_MANAGER
#define FRAO_LITHEKEY_MANAGER_INPUT_MANAGER

// so that we don't get collisions on the name max()
#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <Windows.h>
#include <vector>
#include "..\Types\Base.hpp"
#include "..\Types\Controllers.hpp"

namespace frao
{
inline namespace Input
{
class InputManager  // TODO: rename to InputProxy /
					// InputHandler?
{
	std::vector<std::pair<Strings::widestring,
						  std::shared_ptr<MessageQueue>>>
		m_Queues;
	std::vector<std::pair<InputAction, Message>>
					  m_MessageMap;
	ScreenCoordinates m_MouseHoverLoc;
	// if true, then if no message is found for an action,
	// that action is stripped of it's ActionSpecificData,
	// and the generalised version of the action is tried,
	// instead
	const bool m_Fallback;
	const bool m_LogMisses;

	InputManager(const InputManager&) = delete;
	InputManager& operator=(InputManager&&) = delete;
	InputManager& operator=(const InputManager&) = delete;

	static InputKey getKeyCodeSpecialKeys(
		WPARAM wParam);  // will get keycodes only from
						 // non-character keys
	static InputKey getKeyCodes(
		WPARAM wParam);  // will get keycodes from all keys
						 // on the keyboard
	static InputKey getKeyCodeFromAppCommand(LPARAM lParam);

	void sortQueues() noexcept;
	void sortMap() noexcept;

   public:
	// if 'usefallback' is true, then if no message is found
	// for an action, that action is stripped of it's
	// ActionSpecificData, and the generalised version of
	// the action is tried, instead if 'logmisses' is true,
	// then if no message is found for an action (potentially
	// looking twice, if fallback is active), we will write
	// to the log
	InputManager(bool useFallback, bool logmisses = false);
	InputManager(InputManager&& rhs);
	~InputManager() = default;

	typedef decltype(m_MessageMap)::iterator MapIter;
	typedef decltype(m_Queues)::iterator	 QueueIter;

	QueueIter beginQueues() noexcept;
	QueueIter endQueues() noexcept;

	void addQueue(Strings::widestring			name,
				  std::shared_ptr<MessageQueue> queue);
	void removeQueue(Strings::widestring name) noexcept;
	// will return iter to location *after* removed queue
	QueueIter removeQueue(QueueIter location) noexcept;
	QueueIter getQueue(Strings::widestring name) noexcept;

	MapIter beginMaps() noexcept;
	MapIter endMaps() noexcept;

	void	addMap(InputAction action, Message message);
	MapIter removeMap(MapIter location) noexcept;
	MapIter getMappedMessage(InputAction action) noexcept;

	ScreenCoordinates getLastMouseLoc() const;

	LRESULT messageHandler(HWND window, UINT message,
						   WPARAM wParam, LPARAM lParam);
};
}  // namespace Input
}  // namespace frao

#endif  //! FRAO_LITHEKEY_MANAGER_INPUT_MANAGER