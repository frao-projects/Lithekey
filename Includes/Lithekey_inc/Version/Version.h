#pragma once

#include "LithekeyVersion.h"

#define STRINGISE2(s) #s
#define STRINGISE(s) STRINGISE2(s)

#define VER_FILE					VERSION_MAJOR, VERSION_MINOR, VERSION_STATUS_NUMBER, VERSION_PATCH
#define VER_FILE_DESCRIPTION_STR	"A project for retreiving and coordinating input devices"
#define VER_FILE_STR				STRINGISE(VERSION_MAJOR)	\
									"." STRINGISE(VERSION_MINOR)	\
									"." VERSION_STATUS	\
									"." STRINGISE(VERSION_PATCH)	\

#define VER_PRODUCT					VER_FILE
#define VER_PRODUCT_STR				VER_FILE_STR
#define VER_PRODUCTNAME_STR			"Lithekey"
#define VER_ORIGINAL_FILENAME_STR	"lib__Lithekey.lib"
#define VER_INTERNAL_NAME_STR		VER_ORIGINAL_FILENAME_STR
#define VER_COPYRIGHT_STR			"Copyright (C) 2022 by Freya Rhiannon Mayger"

#ifdef _DEBUG
#define VER_DEBUG					VS_FF_DEBUG
#else
#define VER_DEBUG					0x0L
#endif

#define VER_FILEOS					VOS_NT_WINDOWS32
#define VER_FILEFLAGS				VER_DEBUG | VS_FF_PRERELEASE
#define VER_FILETYPE				VFT_DLL
