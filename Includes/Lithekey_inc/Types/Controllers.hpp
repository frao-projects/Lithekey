#ifndef FRAO_LITHEKEY_TYPES_CONTROLLERS
#define FRAO_LITHEKEY_TYPES_CONTROLLERS

#include <Protean_inc\Structures\CircularBuffer.hpp>
#include <Protean_inc\Functor\Delegate.hpp>
#include <initializer_list>
#include "Base.hpp"

namespace frao
{
inline namespace Input
{
class MessageQueue
{
	CircularBuffer<Message> m_Messages;
	Delegate<void, Message> m_ImportantMessageCallback;
	Message::SubSystem
		m_SubSystemMask;  // will be applied to all pushed
						  // messages

   public:
	MessageQueue(std::initializer_list<Message::SubSystem>
						   subsystems,
				 natural64 queueSize = 1024);
	MessageQueue(Delegate<void, Message> delegate,
				 std::initializer_list<Message::SubSystem>
						   subsystems,
				 natural64 queueSize = 1024);
	virtual ~MessageQueue() = default;

	// will call callback IN ADDITION to pushing, if message
	// is critical!
	void	push(Message input) noexcept;
	Message pop();
	Message top();

	Delegate<void, Message>& getMessageCallback() noexcept;

	natural64 remainingMessages() const noexcept;
};
}  // namespace Input
}  // namespace frao


#endif  //! FRAO_LITHEKEY_TYPES_CONTROLLERS