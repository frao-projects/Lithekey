#ifndef FRAO_LITHEKEY_TYPES_BASE
#define FRAO_LITHEKEY_TYPES_BASE

#include <Nucleus_inc\Environment.hpp>
#include "..\KeyCodes\Codes.hpp"

namespace frao
{
inline namespace Input
{
//==================================
//-----struct ScreenCoordinates-----
//==================================
// class for storing the positions of
// things in a window. ORIGIN IS AT
// THE TOP LEFT OF THE WINDOW
//==================================

struct ScreenCoordinates
{
	small_int16 m_X;
	small_int16 m_Y;

	ScreenCoordinates();
	ScreenCoordinates(small_int16 x, small_int16 y);
	explicit operator bool() const;
};


class ActionSpecificData final
{
	static_assert(
		sizeof(wchar_t) <= sizeof(natural32),
		"character must be able to fit in natural32!");

	// Holds bits of wchar_t, or ScreenCoord x coord in high
	// WORD, and y coord in low WORD
	small_nat32 m_RawData;

   public:
	ActionSpecificData() noexcept;
	ActionSpecificData(ScreenCoordinates coords) noexcept;
	ActionSpecificData(wchar_t character) noexcept;
	ActionSpecificData(natural32 rawbytes) noexcept;

	natural32 getBitfield() const noexcept;

	ScreenCoordinates getAsMouseData() const noexcept;
	wchar_t			  getAsKeyboardData() const noexcept;
};


class InputAction final
{
	// bitfield arrangement, high to low bits:
	//	0-7		|	KeyCode
	//	8-11	|	Modifier
	//	12-15	|	KeyState (DOWN, UP, etc.)
	//	16-31	|	Reserved
	//	32-63	|	Type Specific (wchar_t or ScreenCoords)

	natural64 m_BitField;

   public:
	InputAction();
	InputAction(InputKey keycode, KeyModifier modifier,
				KeyState state, ActionSpecificData data);

	InputKey	getKeyCode() const noexcept;
	KeyModifier getModifiers() const
		noexcept;  // WILL BE OR'ed TOGETHER!
	KeyState		   getKeyState() const noexcept;
	ActionSpecificData getSpecificData() const noexcept;

	bool altIsDown() const noexcept;
	bool controlIsDown() const noexcept;
	bool shiftIsDown() const noexcept;

	bool isMouseType() const noexcept;
	bool isKeyboardType() const noexcept;

	bool operator<(const InputAction& rhs) const noexcept;
	bool operator==(const InputAction& rhs) const noexcept;

	explicit operator bool() const noexcept;
};


class Message final
{
	// bitfield arrangement, high to low bits:
	//	0-15	|	16 BYTES	| Subsystem bitfield (see
	//enum) 	16		|	1 BYTE		| Critical Message (will
	//routed through Delegate, if possible) 	17-31	|	15
	//BYTES	| Subsystem-defined 	32-63	|	32 BYTES	|
	//ActionSpecificData

	natural64 m_BitField;

   public:
	enum class SubSystem : small_nat16
	{
		INVALID		= 0b0000000000000000,
		WINDOW		= 0b0000000000000001,
		TEXT_EDITOR = 0b0000000000000010,
		FLEMON_3	= 0b0000000000000100,
		FLEMON_4	= 0b0000000000001000,
		FLEMON_5	= 0b0000000000010000,
		FLEMON_6	= 0b0000000000100000,
		FLEMON_7	= 0b0000000001000000,
		FLEMON_8	= 0b0000000010000000,
		USER_1		= 0b0000000100000000,
		USER_2		= 0b0000001000000000,
		USER_3		= 0b0000010000000000,
		USER_4		= 0b0000100000000000,
		USER_5		= 0b0001000000000000,
		USER_6		= 0b0010000000000000,
		USER_7		= 0b0100000000000000,
		USER_8		= 0b1000000000000000,
	};

	Message(SubSystem subsystem, small_nat16 subsystemdata,
			bool critical = false) noexcept;
	Message(ActionSpecificData actionspecific,
			SubSystem subsystem, small_nat16 subsystemdata,
			bool critical = false) noexcept;

	void filter(SubSystem subSystemsToKeep) noexcept;

	SubSystem   getSubSystem() const noexcept;
	small_nat16 getSubSystemData() const noexcept;

	ActionSpecificData getSpecificData() const noexcept;
	void			   setSpecificData(
					  ActionSpecificData actionspecific) noexcept;

	bool userDefined() const noexcept;
	bool isCritical() const noexcept;
};
}  // namespace Input
}  // namespace frao

#endif  //! FRAO_LITHEKEY_TYPES_BASE