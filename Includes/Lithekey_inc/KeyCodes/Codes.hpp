#ifndef FRAO_LITHEKEY_KEYCODES_CODES
#define FRAO_LITHEKEY_KEYCODES_CODES

#include <Nucleus_inc\Environment.hpp>

namespace frao
{
inline namespace Input
{
//=========================================================
//----------------Input message enums----------------------
//=========================================================
// these are enums for what key has been pressed, and what
// its current state is
//=========================================================


enum class KeyState : small_nat8
{
	INVALID = 0U,
	DOWN,
	UP,
	DOUBLE_CLICKED,  // for MICE ONLY
	REPEATED		 // for KEYBOARDS ONLY
};

enum class KeyModifier : small_nat8
{
	NONE	= 0x0,
	CONTROL = 0x1,
	ALT		= 0x2,
	SHIFT   = 0x4
};

enum class InputKey : small_nat8
{
	INVALID = 0U,  // must always be first and == 0U
	CHARACTER,
	LEFT_MOUSE,
	MIDDLE_MOUSE,
	RIGHT_MOUSE,
	BACKSPACE,
	TAB,
	ENTER,
	SHIFT,
	CONTROL,
	ALT,
	CAPS_LOCK,
	ESCAPE,
	SPACE,
	PAGE_UP,
	PAGE_DOWN,
	HOME,
	END,
	LEFT_ARROW,
	UP_ARROW,
	RIGHT_ARROW,
	DOWN_ARROW,
	PRINT_SCREEN,
	INSERT,
	KEY_DELETE,
	TOP_0,
	TOP_1,
	TOP_2,
	TOP_3,
	TOP_4,
	TOP_5,
	TOP_6,
	TOP_7,
	TOP_8,
	TOP_9,
	KEY_A,
	KEY_B,
	KEY_C,
	KEY_D,
	KEY_E,
	KEY_F,
	KEY_G,
	KEY_H,
	KEY_I,
	KEY_J,
	KEY_K,
	KEY_L,
	KEY_M,
	KEY_N,
	KEY_O,
	KEY_P,
	KEY_Q,
	KEY_R,
	KEY_S,
	KEY_T,
	KEY_U,
	KEY_V,
	KEY_W,
	KEY_X,
	KEY_Y,
	KEY_Z,
	RIGHT_0,
	RIGHT_1,
	RIGHT_2,
	RIGHT_3,
	RIGHT_4,
	RIGHT_5,
	RIGHT_6,
	RIGHT_7,
	RIGHT_8,
	RIGHT_9,
	KEY_MULTIPLY,
	KEY_ADD,
	KEY_SUBTRACT,
	KEY_DECIMAL,  // the num pad del/'.' button
	KEY_DIVIDE,
	KEY_F1,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	KEY_F10,
	KEY_F11,
	KEY_F12,
	MUTE,
	NEXT_TRACK,
	LAST_TRACK,
	PLAY_PAUSE,
	VOLUME_DOWN,
	VOLUME_UP,
	COPY,   // from an appcommand
	CUT,	// from an appcommand
	FIND,   // from an appcommand
	PASTE,  // from an appcommand
	SAVE,   // from an appcommand
	UNDO,   // from an appcommand
	COMMA,
	FULL_STOP,
	MAX_VALUE  // must always be last in the list
};
}  // namespace Input
}  // namespace frao

#endif  //! FRAO_LITHEKEY_KEYCODES_CODES