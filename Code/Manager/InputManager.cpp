#include "Manager\InputManager.hpp"
#include <windowsx.h>
#include <Nucleus_inc\Environment.hpp>

namespace frao
{
inline namespace Input
{
using namespace Strings::Operators;


// InputProxy functions
// private
InputKey InputManager::getKeyCodeSpecialKeys(WPARAM wParam)
{
	InputKey key;

	switch (wParam)
	{
		case VK_BACK:
			key = InputKey::BACKSPACE;
			break;
		case VK_TAB:
			key = InputKey::TAB;
			break;
		case VK_SHIFT:
		case VK_LSHIFT:
		case VK_RSHIFT:
			key = InputKey::SHIFT;
			break;
		case VK_CONTROL:
		case VK_LCONTROL:
		case VK_RCONTROL:
			key = InputKey::CONTROL;
			break;
		case VK_MENU:
		case VK_LMENU:
		case VK_RMENU:
			key = InputKey::ALT;
			break;
		case VK_CAPITAL:
			key = InputKey::CAPS_LOCK;
			break;
		case VK_ESCAPE:
			key = InputKey::ESCAPE;
			break;
		case VK_PRIOR:
			key = InputKey::PAGE_UP;
			break;
		case VK_NEXT:
			key = InputKey::PAGE_DOWN;
			break;
		case VK_HOME:
			key = InputKey::HOME;
			break;
		case VK_END:
			key = InputKey::END;
			break;
		case VK_LEFT:
			key = InputKey::LEFT_ARROW;
			break;
		case VK_UP:
			key = InputKey::UP_ARROW;
			break;
		case VK_RIGHT:
			key = InputKey::RIGHT_ARROW;
			break;
		case VK_DOWN:
			key = InputKey::DOWN_ARROW;
			break;
		case VK_SNAPSHOT:
			key = InputKey::PRINT_SCREEN;
			break;
		case VK_INSERT:
			key = InputKey::INSERT;
			break;
		case VK_DELETE:
			key = InputKey::KEY_DELETE;
			break;
		case VK_NUMPAD0:
			key = InputKey::RIGHT_0;
			break;
		case VK_NUMPAD1:
			key = InputKey::RIGHT_1;
			break;
		case VK_NUMPAD2:
			key = InputKey::RIGHT_2;
			break;
		case VK_NUMPAD3:
			key = InputKey::RIGHT_3;
			break;
		case VK_NUMPAD4:
			key = InputKey::RIGHT_4;
			break;
		case VK_NUMPAD5:
			key = InputKey::RIGHT_5;
			break;
		case VK_NUMPAD6:
			key = InputKey::RIGHT_6;
			break;
		case VK_NUMPAD7:
			key = InputKey::RIGHT_7;
			break;
		case VK_NUMPAD8:
			key = InputKey::RIGHT_8;
			break;
		case VK_NUMPAD9:
			key = InputKey::RIGHT_9;
			break;
		case VK_F1:
			key = InputKey::KEY_F1;
			break;
		case VK_F2:
			key = InputKey::KEY_F2;
			break;
		case VK_F3:
			key = InputKey::KEY_F3;
			break;
		case VK_F4:
			key = InputKey::KEY_F4;
			break;
		case VK_F5:
			key = InputKey::KEY_F5;
			break;
		case VK_F6:
			key = InputKey::KEY_F6;
			break;
		case VK_F7:
			key = InputKey::KEY_F7;
			break;
		case VK_F8:
			key = InputKey::KEY_F8;
			break;
		case VK_F9:
			key = InputKey::KEY_F9;
			break;
		case VK_F10:
			key = InputKey::KEY_F10;
			break;
		case VK_F11:
			key = InputKey::KEY_F11;
			break;
		case VK_F12:
			key = InputKey::KEY_F12;
			break;
		case VK_VOLUME_MUTE:
			key = InputKey::MUTE;
			break;
		case VK_MEDIA_NEXT_TRACK:
			key = InputKey::NEXT_TRACK;
			break;
		case VK_MEDIA_PREV_TRACK:
			key = InputKey::LAST_TRACK;
			break;
		case VK_MEDIA_PLAY_PAUSE:
		case VK_PLAY:
		case VK_PAUSE:
			key = InputKey::PLAY_PAUSE;
			break;
		case VK_VOLUME_DOWN:
			key = InputKey::VOLUME_DOWN;
			break;
		case VK_VOLUME_UP:
			key = InputKey::VOLUME_UP;
			break;
		default:
			key = InputKey::INVALID;
	}

	return key;
}
InputKey InputManager::getKeyCodes(WPARAM wParam)
{
	InputKey key;

	switch (wParam)
	{
		case VK_BACK:
			key = InputKey::BACKSPACE;
			break;
		case VK_TAB:
			key = InputKey::TAB;
			break;
		case VK_RETURN:
			key = InputKey::ENTER;
			break;
		case VK_SHIFT:
		case VK_LSHIFT:
		case VK_RSHIFT:
			key = InputKey::SHIFT;
			break;
		case VK_CONTROL:
		case VK_LCONTROL:
		case VK_RCONTROL:
			key = InputKey::CONTROL;
			break;
		case VK_MENU:
		case VK_LMENU:
		case VK_RMENU:
			key = InputKey::ALT;
			break;
		case VK_CAPITAL:
			key = InputKey::CAPS_LOCK;
			break;
		case VK_ESCAPE:
			key = InputKey::ESCAPE;
			break;
		case VK_SPACE:
			key = InputKey::SPACE;
			break;
		case VK_PRIOR:
			key = InputKey::PAGE_UP;
			break;
		case VK_NEXT:
			key = InputKey::PAGE_DOWN;
			break;
		case VK_HOME:
			key = InputKey::HOME;
			break;
		case VK_END:
			key = InputKey::END;
			break;
		case VK_LEFT:
			key = InputKey::LEFT_ARROW;
			break;
		case VK_UP:
			key = InputKey::UP_ARROW;
			break;
		case VK_RIGHT:
			key = InputKey::RIGHT_ARROW;
			break;
		case VK_DOWN:
			key = InputKey::DOWN_ARROW;
			break;
		case VK_SNAPSHOT:
			key = InputKey::PRINT_SCREEN;
			break;
		case VK_INSERT:
			key = InputKey::INSERT;
			break;
		case VK_DELETE:
			key = InputKey::KEY_DELETE;
			break;
		case 0x30:
			key = InputKey::TOP_0;
			break;
		case 0x31:
			key = InputKey::TOP_1;
			break;
		case 0x32:
			key = InputKey::TOP_2;
			break;
		case 0x33:
			key = InputKey::TOP_3;
			break;
		case 0x34:
			key = InputKey::TOP_4;
			break;
		case 0x35:
			key = InputKey::TOP_5;
			break;
		case 0x36:
			key = InputKey::TOP_6;
			break;
		case 0x37:
			key = InputKey::TOP_7;
			break;
		case 0x38:
			key = InputKey::TOP_8;
			break;
		case 0x39:
			key = InputKey::TOP_9;
			break;
		case 0x41:
			key = InputKey::KEY_A;
			break;
		case 0x42:
			key = InputKey::KEY_B;
			break;
		case 0x43:
			key = InputKey::KEY_C;
			break;
		case 0x44:
			key = InputKey::KEY_D;
			break;
		case 0x45:
			key = InputKey::KEY_E;
			break;
		case 0x46:
			key = InputKey::KEY_F;
			break;
		case 0x47:
			key = InputKey::KEY_G;
			break;
		case 0x48:
			key = InputKey::KEY_H;
			break;
		case 0x49:
			key = InputKey::KEY_I;
			break;
		case 0x4A:
			key = InputKey::KEY_J;
			break;
		case 0x4B:
			key = InputKey::KEY_K;
			break;
		case 0x4C:
			key = InputKey::KEY_L;
			break;
		case 0x4D:
			key = InputKey::KEY_M;
			break;
		case 0x4E:
			key = InputKey::KEY_N;
			break;
		case 0x4F:
			key = InputKey::KEY_O;
			break;
		case 0x50:
			key = InputKey::KEY_P;
			break;
		case 0x51:
			key = InputKey::KEY_Q;
			break;
		case 0x52:
			key = InputKey::KEY_R;
			break;
		case 0x53:
			key = InputKey::KEY_S;
			break;
		case 0x54:
			key = InputKey::KEY_T;
			break;
		case 0x55:
			key = InputKey::KEY_U;
			break;
		case 0x56:
			key = InputKey::KEY_V;
			break;
		case 0x57:
			key = InputKey::KEY_W;
			break;
		case 0x58:
			key = InputKey::KEY_X;
			break;
		case 0x59:
			key = InputKey::KEY_Y;
			break;
		case 0x5A:
			key = InputKey::KEY_Z;
			break;
		case VK_NUMPAD0:
			key = InputKey::RIGHT_0;
			break;
		case VK_NUMPAD1:
			key = InputKey::RIGHT_1;
			break;
		case VK_NUMPAD2:
			key = InputKey::RIGHT_2;
			break;
		case VK_NUMPAD3:
			key = InputKey::RIGHT_3;
			break;
		case VK_NUMPAD4:
			key = InputKey::RIGHT_4;
			break;
		case VK_NUMPAD5:
			key = InputKey::RIGHT_5;
			break;
		case VK_NUMPAD6:
			key = InputKey::RIGHT_6;
			break;
		case VK_NUMPAD7:
			key = InputKey::RIGHT_7;
			break;
		case VK_NUMPAD8:
			key = InputKey::RIGHT_8;
			break;
		case VK_NUMPAD9:
			key = InputKey::RIGHT_9;
			break;
		case VK_MULTIPLY:
			key = InputKey::KEY_MULTIPLY;
			break;
		case VK_ADD:
			key = InputKey::KEY_ADD;
			break;
		case VK_SUBTRACT:
			key = InputKey::KEY_SUBTRACT;
			break;
		case VK_DECIMAL:
			key = InputKey::KEY_DECIMAL;
			break;
		case VK_DIVIDE:
			key = InputKey::KEY_DIVIDE;
			break;
		case VK_F1:
			key = InputKey::KEY_F1;
			break;
		case VK_F2:
			key = InputKey::KEY_F2;
			break;
		case VK_F3:
			key = InputKey::KEY_F3;
			break;
		case VK_F4:
			key = InputKey::KEY_F4;
			break;
		case VK_F5:
			key = InputKey::KEY_F5;
			break;
		case VK_F6:
			key = InputKey::KEY_F6;
			break;
		case VK_F7:
			key = InputKey::KEY_F7;
			break;
		case VK_F8:
			key = InputKey::KEY_F8;
			break;
		case VK_F9:
			key = InputKey::KEY_F9;
			break;
		case VK_F10:
			key = InputKey::KEY_F10;
			break;
		case VK_F11:
			key = InputKey::KEY_F11;
			break;
		case VK_F12:
			key = InputKey::KEY_F12;
			break;
		case VK_VOLUME_MUTE:
			key = InputKey::MUTE;
			break;
		case VK_MEDIA_NEXT_TRACK:
			key = InputKey::NEXT_TRACK;
			break;
		case VK_MEDIA_PREV_TRACK:
			key = InputKey::LAST_TRACK;
			break;
		case VK_MEDIA_PLAY_PAUSE:
		case VK_PLAY:
		case VK_PAUSE:
			key = InputKey::PLAY_PAUSE;
			break;
		case VK_VOLUME_DOWN:
			key = InputKey::VOLUME_DOWN;
			break;
		case VK_VOLUME_UP:
			key = InputKey::VOLUME_UP;
			break;
		case VK_OEM_COMMA:
			key = InputKey::COMMA;
			break;
		case VK_OEM_PERIOD:
			key = InputKey::FULL_STOP;
			break;
		case VK_LBUTTON:
			key = InputKey::LEFT_MOUSE;
			break;
		case VK_MBUTTON:
			key = InputKey::MIDDLE_MOUSE;
			break;
		case VK_RBUTTON:
			key = InputKey::RIGHT_MOUSE;
			break;
		default:
			key = InputKey::INVALID;
	}

	return key;
}
InputKey InputManager::getKeyCodeFromAppCommand(LPARAM lParam)
{
	natural  rawcode = GET_APPCOMMAND_LPARAM(lParam);
	InputKey result;

	switch (rawcode)
	{
		case APPCOMMAND_COPY:
			result = InputKey::COPY;
			break;
		case APPCOMMAND_CUT:
			result = InputKey::CUT;
			break;
		case APPCOMMAND_FIND:
			result = InputKey::FIND;
			break;
		case APPCOMMAND_MEDIA_NEXTTRACK:
			result = InputKey::NEXT_TRACK;
			break;
		case APPCOMMAND_MEDIA_PREVIOUSTRACK:
			result = InputKey::LAST_TRACK;
			break;
		case APPCOMMAND_MEDIA_PAUSE:
		case APPCOMMAND_MEDIA_PLAY:
		case APPCOMMAND_MEDIA_PLAY_PAUSE:
			result = InputKey::PLAY_PAUSE;
			break;
		case APPCOMMAND_PASTE:
			result = InputKey::PASTE;
			break;
		case APPCOMMAND_SAVE:
			result = InputKey::SAVE;
			break;
		case APPCOMMAND_UNDO:
			result = InputKey::UNDO;
			break;
		case APPCOMMAND_VOLUME_DOWN:
			result = InputKey::VOLUME_DOWN;
			break;
		case APPCOMMAND_VOLUME_UP:
			result = InputKey::VOLUME_UP;
			break;
		case APPCOMMAND_VOLUME_MUTE:
			result = InputKey::MUTE;
			break;
		default:
			result = InputKey::INVALID;
			break;
	}

	return result;
}
//
void InputManager::sortQueues() noexcept
{
	typedef std::pair<Strings::widestring,
					  std::shared_ptr<MessageQueue>>
		 QueueType;
	auto queueSortLambda = [](const QueueType& lhs,
							  const QueueType& rhs) -> bool {
		return lhs.first < rhs.first;
	};

	std::sort(m_Queues.begin(), m_Queues.end(), queueSortLambda);
}
void InputManager::sortMap() noexcept
{
	typedef std::pair<InputAction, Message> MapType;
	auto mapSortLambda = [](const MapType& lhs,
							const MapType& rhs) -> bool {
		return lhs.first < rhs.first;
	};

	std::sort(m_MessageMap.begin(), m_MessageMap.end(),
			  mapSortLambda);
}
// public
InputManager::InputManager(bool useFallback, bool logmisses)
	: m_Queues(),
	  m_MessageMap(),
	  m_MouseHoverLoc(-1, -1),
	  m_Fallback(useFallback),
	  m_LogMisses(logmisses)
{}
InputManager::InputManager(InputManager&& rhs)
	: m_Queues(std::move(rhs.m_Queues)),
	  m_MessageMap(std::move(rhs.m_MessageMap)),
	  m_MouseHoverLoc(std::move(rhs.m_MouseHoverLoc)),
	  m_Fallback(rhs.m_Fallback),
	  m_LogMisses(rhs.m_LogMisses)
{}
//
InputManager::QueueIter InputManager::beginQueues() noexcept
{
	return m_Queues.begin();
}
InputManager::QueueIter InputManager::endQueues() noexcept
{
	return m_Queues.end();
}
//
void InputManager::addQueue(Strings::widestring			  name,
							std::shared_ptr<MessageQueue> queue)
{
	runtime_assert(name.size() != 0);
	runtime_assert(queue);

	m_Queues.emplace_back(std::move(name), queue);
	sortQueues();
}
void InputManager::removeQueue(Strings::widestring name) noexcept
{
	auto location = getQueue(std::move(name));

	if (location != m_Queues.end())
	{
		removeQueue(location);
	}
}
InputManager::QueueIter InputManager::removeQueue(
	QueueIter location) noexcept
{
	if (location != m_Queues.end())
	{
		return m_Queues.erase(location);
	} else
	{
		return m_Queues.end();
	}
}
InputManager::QueueIter InputManager::getQueue(
	Strings::widestring name) noexcept
{
	// loop until past name, and return end if nothing found
	auto iter = m_Queues.begin();
	while (iter != m_Queues.end())
	{
		if (iter->first < name)
		{
			++iter;
		}

		if (iter->first == name)
		{
			break;
		} else
		{
			getErrorLog()->addError<NotFound>(
				FILELOC_UTF8,
				u8"No such Queue, with name: "_ustring + name);
			iter = m_Queues.end();
		}
	}

	return iter;
}
//
InputManager::MapIter InputManager::beginMaps() noexcept
{
	return m_MessageMap.begin();
}
InputManager::MapIter InputManager::endMaps() noexcept
{
	return m_MessageMap.end();
}
//
void InputManager::addMap(InputAction action, Message message)
{
	runtime_assert(action);

	m_MessageMap.emplace_back(action, message);
	sortMap();
}
InputManager::MapIter InputManager::removeMap(
	MapIter location) noexcept
{
	if (location != m_MessageMap.end())
	{
		return m_MessageMap.erase(location);
	} else
	{
		return m_MessageMap.end();
	}
}
InputManager::MapIter InputManager::getMappedMessage(
	InputAction action) noexcept
{
	runtime_assert(action);

	// loop until past name, and return end if nothing found
	auto iter = m_MessageMap.begin();
	while (iter != m_MessageMap.end())
	{
		if (iter->first < action)
		{
			++iter;
			continue;
		}

		if (iter->first == action)
		{
			break;
		} else
		{
			iter = m_MessageMap.end();
		}
	}

	return iter;
}
//
ScreenCoordinates InputManager::getLastMouseLoc() const
{
	return m_MouseHoverLoc;
}
//
LRESULT InputManager::messageHandler(HWND window, UINT message,
									 WPARAM wParam, LPARAM lParam)
{
	LRESULT					 result   = 0;
	KeyModifier				 modifier = KeyModifier::NONE;
	std::vector<InputAction> actions;

	if ((GetKeyState(VK_CONTROL) & 0x8000) != 0)
		modifier = static_cast<KeyModifier>(
			static_cast<natural>(modifier)
			| static_cast<natural>(KeyModifier::CONTROL));

	if ((GetKeyState(VK_MENU) & 0x8000) != 0)
		modifier = static_cast<KeyModifier>(
			static_cast<natural>(modifier)
			| static_cast<natural>(KeyModifier::ALT));

	if ((GetKeyState(VK_SHIFT) & 0x8000) != 0)
		modifier = static_cast<KeyModifier>(
			static_cast<natural>(modifier)
			| static_cast<natural>(KeyModifier::SHIFT));

	switch (message)
	{
		case WM_LBUTTONDOWN:
			actions.emplace_back(
				InputKey::LEFT_MOUSE, modifier, KeyState::DOWN,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_LBUTTONUP:
			actions.emplace_back(
				InputKey::LEFT_MOUSE, modifier, KeyState::UP,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_LBUTTONDBLCLK:
			actions.emplace_back(
				InputKey::LEFT_MOUSE, modifier,
				KeyState::DOUBLE_CLICKED,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_MBUTTONDOWN:
			actions.emplace_back(
				InputKey::MIDDLE_MOUSE, modifier, KeyState::DOWN,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_MBUTTONUP:
			actions.emplace_back(
				InputKey::MIDDLE_MOUSE, modifier, KeyState::UP,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_MBUTTONDBLCLK:
			actions.emplace_back(
				InputKey::MIDDLE_MOUSE, modifier,
				KeyState::DOUBLE_CLICKED,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_RBUTTONDOWN:
			actions.emplace_back(
				InputKey::RIGHT_MOUSE, modifier, KeyState::DOWN,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_RBUTTONUP:
			actions.emplace_back(
				InputKey::RIGHT_MOUSE, modifier, KeyState::UP,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_RBUTTONDBLCLK:
			actions.emplace_back(
				InputKey::RIGHT_MOUSE, modifier,
				KeyState::DOUBLE_CLICKED,
				ActionSpecificData(ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)))));
			break;

		case WM_MOUSEMOVE:
			m_MouseHoverLoc = ScreenCoordinates(
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 0)),
					static_cast<small_int16>(
						ExtractBits(lParam, 16, 16)));
			break;

		case WM_KEYDOWN:  // Deliberate fall-through
		case WM_SYSKEYDOWN:
		{
			// get the amount of repeats of the key. Is in
			// the bottom 16 bits of the LPARAM
			small_nat16 repeats = static_cast<small_nat16>(
				ExtractBits(static_cast<natural64>(lParam), 16, 0));
			InputKey key = getKeyCodes(wParam);

			// if (m_FullChars &&
			// ((static_cast<small_nat16>(modifier) 		&
			// static_cast<small_nat16>(KeyModifier::CONTROL))
			//== 0)) 	key = getKeyCodeSpecialKeys(wParam);
			// else
			//	key = getKeyCodes(wParam);

			if (key != InputKey::INVALID)
			{
				if (repeats <= 1)
				{
					actions.emplace_back(key, modifier,
										 KeyState::DOWN,
										 ActionSpecificData(0U));
				} else
				{
					while (repeats != 0)
					{
						actions.emplace_back(key, modifier,
											 KeyState::REPEATED,
											 ActionSpecificData(0U));
						--repeats;
					}
				}
			} else
			{
				result =
					DefWindowProcW(window, message, wParam, lParam);
			}
		}
		break;
		case WM_KEYUP:  // Deliberate fall-through
		case WM_SYSKEYUP:
		{
			InputKey key = getKeyCodes(wParam);

			// if (m_FullChars &&
			// ((static_cast<small_nat16>(modifier) 		&
			// static_cast<small_nat16>(KeyModifier::CONTROL))
			//== 0)) 	key = getKeyCodeSpecialKeys(wParam);
			// else
			//	key = getKeyCodes(wParam);

			if (key != InputKey::INVALID)
			{
				actions.emplace_back(key, modifier, KeyState::UP,
									 ActionSpecificData(0U));
			} else
			{
				result =
					DefWindowProcW(window, message, wParam, lParam);
			}
		}
		break;
		case WM_APPCOMMAND:
		{
			InputKey key = getKeyCodeFromAppCommand(lParam);

			if (key != InputKey::INVALID)
			{
				actions.emplace_back(key, modifier, KeyState::DOWN,
									 ActionSpecificData(0U));
				result = TRUE;
			} else
			{
				result =
					DefWindowProcW(window, message, wParam, lParam);
			}
		}
		break;
		case WM_CHAR:  // Deliberate fall-through
		case WM_SYSCHAR:
		{
			// get the amount of repeats of the key. Is in
			// the bottom 16 bits of the LPARAM
			small_nat16 repeats = static_cast<small_nat16>(
				ExtractBits(static_cast<natural64>(lParam), 16, 0));

			if (repeats <= 1)
			{
				actions.emplace_back(InputKey::CHARACTER, modifier,
									 KeyState::DOWN,
									 static_cast<wchar_t>(wParam));
			} else
			{
				while (repeats != 0)
				{
					actions.emplace_back(
						InputKey::CHARACTER, modifier,
						KeyState::REPEATED,
						static_cast<wchar_t>(wParam));
					--repeats;
				}
			}
		}
		break;
		default:
			// ignore, because we want people to be able to
			// just pass us all messages, and we filter the
			// ones we want
			return DefWindowProcW(window, message, wParam, lParam);
	}

	// push the message to all queues
	for (auto action : actions)
	{
		auto location = getMappedMessage(action);

		if (m_Fallback && (location == m_MessageMap.end()))
		{
			// ie: get rid of specific condition, because
			// fallback is enabled
			location = getMappedMessage(InputAction{
				action.getKeyCode(), action.getModifiers(),
				action.getKeyState(), ActionSpecificData{}});
		}

		if (location != m_MessageMap.end())
		{
			auto sendmessage = location->second;
			sendmessage.setSpecificData(action.getSpecificData());

			for (auto& pair : m_Queues)
			{
				pair.second->push(sendmessage);
			}
		} else if (m_LogMisses)
		{
			// then we have decided to log action misses, so
			// add error, because both options found nothing
			getErrorLog()->addError<NotFound>(
				FILELOC_UTF8,
				u8"No such InputAction, with code: "_ustring
					+ Strings::utf8string{static_cast<natural>(
						  action.getKeyCode())}
					+ u8" state: "_ustring
					+ Strings::utf8string{static_cast<natural>(
						  action.getKeyState())}
					+ u8" modifiers: "_ustring
					+ Strings::utf8string{static_cast<natural>(
						  action.getModifiers())}
					+ u8" and specific data: "_ustring
					+ Strings::utf8string{static_cast<natural>(
						  action.getSpecificData().getBitfield())});
		}
	}

	return result;
}
}  // namespace Input
}  // namespace frao