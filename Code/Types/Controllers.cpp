#include "Types\Controllers.hpp"
#include <Nucleus_inc\Error.hpp>

namespace frao
{
inline namespace Input
{
using namespace Strings::Operators;


// MessageQueue functions
// public
MessageQueue::MessageQueue(
	std::initializer_list<Message::SubSystem> subsystems,
	natural64								  queueSize)
	: m_Messages(queueSize),
	  m_ImportantMessageCallback(),
	  m_SubSystemMask(Message::SubSystem::INVALID)
{
	for (auto subsystem : subsystems)
	{
		m_SubSystemMask = static_cast<Message::SubSystem>(
			static_cast<small_nat16>(m_SubSystemMask)
			| static_cast<small_nat16>(subsystem));
	}
}
MessageQueue::MessageQueue(
	Delegate<void, Message>					  delegate,
	std::initializer_list<Message::SubSystem> subsystems,
	natural64								  queueSize)
	: MessageQueue(subsystems, queueSize)
{
	m_ImportantMessageCallback = std::move(delegate);
}
//
void MessageQueue::push(Message input) noexcept
{
	input.filter(m_SubSystemMask);

	if (input.getSubSystem() != Message::SubSystem::INVALID)
	{
		if (input.isCritical())
		{
			// callback will just fail to call anything, if
			// empty. Therefore, theer is no need to check
			m_ImportantMessageCallback(input);
		}

		m_Messages.append(input);
	}
}
Message MessageQueue::pop()
{
	Message result = top();
	m_Messages.pop();

	return result;
}
Message MessageQueue::top()
{
	if (m_Messages.runningSize() != 0)
	{
		return m_Messages.get(0);
	} else
	{
		throw getErrorLog()->createException<NotFound>(
			FILELOC_UTF8,
			u8"Cannot return top of queue: queue is empty!"_ustring);
	}
}
//
Delegate<void, Message>& MessageQueue::getMessageCallback() noexcept
{
	return m_ImportantMessageCallback;
}
//
natural64 MessageQueue::remainingMessages() const noexcept
{
	return m_Messages.runningSize();
}
}  // namespace Input
}  // namespace frao