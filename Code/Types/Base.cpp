#include <Nucleus_inc\Environment.hpp>
#include "Types\Base.hpp"

namespace frao
{
inline namespace Input
{

//==================================
//-----struct ScreenCoordinates-----
//==================================
// class for storing the positions of
// things in a window. ORIGIN IS AT
// THE TOP LEFT OF THE WINDOW
//==================================

ScreenCoordinates::ScreenCoordinates() : m_X(-1), m_Y(-1)
{}
ScreenCoordinates::ScreenCoordinates(small_int16 x,
									 small_int16 y)
	: m_X(x), m_Y(y)
{}
//
ScreenCoordinates::operator bool() const
{
	return ((m_X >= 0) && (m_Y >= 0));
}


// ActionSpecifiedData functions
// public
ActionSpecificData::ActionSpecificData() noexcept
	: ActionSpecificData(0U)
{}
ActionSpecificData::ActionSpecificData(
	ScreenCoordinates coords) noexcept
	: m_RawData((static_cast<small_nat32>(
					 static_cast<small_nat16>(coords.m_X))
				 << 16)
				| static_cast<small_nat32>(
					  static_cast<small_nat16>(coords.m_Y)))
{}
ActionSpecificData::ActionSpecificData(
	wchar_t character) noexcept
	: m_RawData(static_cast<small_nat32>(character))
{}
ActionSpecificData::ActionSpecificData(
	natural32 rawbytes) noexcept
	: m_RawData(rawbytes)
{}
//
natural32 ActionSpecificData::getBitfield() const noexcept
{
	return static_cast<natural32>(m_RawData);
}
//
ScreenCoordinates ActionSpecificData::getAsMouseData() const
	noexcept
{
	return ScreenCoordinates{
		static_cast<small_int16>((m_RawData & 0xFFFF0000)
								 >> 16),
		static_cast<small_int16>(m_RawData & 0xFFFF)};
}
wchar_t ActionSpecificData::getAsKeyboardData() const
	noexcept
{
	return static_cast<wchar_t>(m_RawData);
}


// InputAction functions
// public
InputAction::InputAction() : m_BitField(0)
{}
InputAction::InputAction(InputKey			keycode,
						 KeyModifier		modifier,
						 KeyState			state,
						 ActionSpecificData data)
	: m_BitField(
		  (static_cast<natural64>(keycode) << 56)
		  | (static_cast<natural64>(modifier) << 52)
		  | (static_cast<natural64>(state) << 48)
		  | static_cast<natural64>(data.getBitfield()))
{
	constexpr const natural64 datamax = 1ULL << 32ULL;

	runtime_assert(static_cast<natural64>(state)
				   < (1ULL << 4ULL));  // (1 << 4) ==  16
	runtime_assert(static_cast<natural64>(modifier)
				   < (1ULL << 4ULL));  // (1 << 4) ==  16
	runtime_assert(static_cast<natural64>(keycode)
				   < (1ULL << 8ULL));  // (1 << 8) ==  256
	runtime_assert(
		static_cast<natural64>(data.getBitfield())
		< datamax);  // (1 << 32) ==  2^32
}
//
InputKey InputAction::getKeyCode() const noexcept
{
	return static_cast<InputKey>(
		(m_BitField & 0xFF00000000000000) >> 56);
}
KeyModifier InputAction::getModifiers() const noexcept
{
	return static_cast<KeyModifier>(
		(m_BitField & 0xF0000000000000) >> 52);
}
KeyState InputAction::getKeyState() const noexcept
{
	return static_cast<KeyState>(
		(m_BitField & 0xF000000000000) >> 48);
}
ActionSpecificData InputAction::getSpecificData() const
	noexcept
{
	return ActionSpecificData(
		static_cast<natural32>(m_BitField & 0xFFFFFFFF));
}
//
bool InputAction::altIsDown() const noexcept
{
	return ((m_BitField & 0xF0000000000000) >> 52)
		   & static_cast<natural64>(KeyModifier::ALT);
}
bool InputAction::controlIsDown() const noexcept
{
	return ((m_BitField & 0xF0000000000000) >> 52)
		   & static_cast<natural64>(KeyModifier::CONTROL);
}
bool InputAction::shiftIsDown() const noexcept
{
	return ((m_BitField & 0xF0000000000000) >> 52)
		   & static_cast<natural64>(KeyModifier::SHIFT);
}
//
bool InputAction::isMouseType() const noexcept
{
	switch (getKeyCode())
	{
		case InputKey::LEFT_MOUSE:
		case InputKey::MIDDLE_MOUSE:
		case InputKey::RIGHT_MOUSE:
			return true;
		default:
			return false;
	}
}
bool InputAction::isKeyboardType() const noexcept
{
	return !isMouseType();
}
//
bool InputAction::operator<(const InputAction& rhs) const
	noexcept
{
	return m_BitField < rhs.m_BitField;
}
bool InputAction::operator==(const InputAction& rhs) const
	noexcept
{
	return m_BitField == rhs.m_BitField;
}
InputAction::operator bool() const noexcept
{
	return m_BitField != 0;
}


// Message functions
// public
Message::Message(SubSystem   subsystem,
				 small_nat16 subsystemdata,
				 bool		 critical) noexcept
	: m_BitField((static_cast<natural64>(subsystem) << 48)
				 | ((critical) ? (0x800000000000) : 0x0)
				 | ((static_cast<natural64>(subsystemdata)
					 & 0x7FFF)
					<< 32))
{}
Message::Message(ActionSpecificData actionspecific,
				 SubSystem			subsystem,
				 small_nat16		subsystemdata,
				 bool				critical) noexcept
	: Message(subsystem, subsystemdata, critical)
{
	m_BitField |= actionspecific.getBitfield();
}
//
void Message::filter(
	Message::SubSystem subSystemsToKeep) noexcept
{
	// keep specified subsystems, and all surrounding data
	// (extra leading zeros in hex value for legibility)
	m_BitField &=
		(0x0000FFFFFFFFFFFF
		 | (static_cast<natural64>(subSystemsToKeep)
			<< 48));
}
//
Message::SubSystem Message::getSubSystem() const noexcept
{
	return static_cast<SubSystem>(
		(m_BitField & 0xFFFF000000000000) >> 48);
}
small_nat16 Message::getSubSystemData() const noexcept
{
	return static_cast<small_nat16>(
		(m_BitField & 0x7FFF00000000) >> 32);
}
//
ActionSpecificData Message::getSpecificData() const noexcept
{
	return ActionSpecificData(
		static_cast<natural32>(m_BitField & 0xFFFFFFFF));
}
void Message::setSpecificData(
	ActionSpecificData actionspecific) noexcept
{
	// remove previous specific data, and or new in
	m_BitField = (m_BitField & 0xFFFFFFFF00000000)
				 | static_cast<natural64>(
					   actionspecific.getBitfield());
}
//
bool Message::userDefined() const noexcept
{
	constexpr const natural64 mask = 0xFFULL << 56ULL;
	return (m_BitField & mask)
		   != 0;  // is any of the top BYTE set?
}
bool Message::isCritical() const noexcept
{
	return (m_BitField & 0x800000000000)
		   != 0;  // ie: is bit #16 set?
}
}
}